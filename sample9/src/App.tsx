import React, { useRef } from 'react';
import * as d3 from 'd3';
import './App.css';

import MapSample from './MapSample';

const App: React.FC = () => {
  const node = useRef(null);

  // const projection =  d3.geoEquirectangular()
  // const projection = d3.geoMercator()
  const projection = d3.geoOrthographic()
    .clipAngle(180)
    .scale(200);

  return (
    <div ref={node}>
        <MapSample
          mapurl="ne_110m_admin_0_countries_topo_0.50.json"
          dataurl="ne_50m_populated_places_topo.json"
          objectsname="countries"
          projection={projection}
          latitude={139}
          longitude={35}></MapSample>
    </div>
  );
}

export default App;
