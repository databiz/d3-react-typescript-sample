import React, { FC, useState } from 'react';
import { Feature, FeatureCollection } from 'geojson';
import * as d3 from 'd3';
import { GeoPermissibleObjects } from 'd3';
import PureCanvas, { initCanvas } from './PureCanvas';

interface Prop {
  projection: d3.GeoProjection;
  features: Feature | FeatureCollection;
}

interface State {
  context?: CanvasRenderingContext2D;
}

const ArcLine:FC<Prop> = (props) => {
  const [state, setState] = useState<State>({});

  const saveRef = (canvas:HTMLCanvasElement) => {
    const context = initCanvas(canvas);
    if (context) {
      setState({
        ...state,
        context: context
      });
    }
  }

  if (state.context) {
    const features:Feature[] = (props.features.type === "FeatureCollection") ?
      props.features.features : [props.features];

    state.context.save();
    state.context.clearRect(0, 0, state.context.canvas.clientWidth, state.context.canvas.clientHeight);
    createPoint(state.context, features, props.projection);
    createArcLine(state.context, features, props.projection);
  }

  return <PureCanvas getRef={saveRef}></PureCanvas>
}

const createPoint = (ctx: CanvasRenderingContext2D, cityFeatures:Feature[],
  projection: d3.GeoProjection) => {
  const [min, max] = d3.extent(cityFeatures, (f):number => {
    return (f.properties) ? f.properties.POP_MAX : 0;
  });
  const scale = (min !== undefined && max !== undefined)
    ? d3.scaleLinear().domain([min, max]).range([0, 10])
    : null;
  if (!scale) return;
  const pathGenerator = d3.geoPath<void, GeoPermissibleObjects>()
    .projection(projection).context(ctx);
  cityFeatures.forEach((f) => {
    if(f.geometry.type === "Point" && f.properties) {
      const p = projection([f.geometry.coordinates[0], f.geometry.coordinates[1]]);
      if(!p) return;
      ctx.fillStyle = "yellow";
      ctx.strokeStyle = "none";
      ctx.beginPath();
      pathGenerator.pointRadius(scale(f.properties.POP_MAX))(f);
      ctx.arc(p[0], p[1], scale(f.properties.POP_MAX), 0, Math.PI*2, false);
      ctx.fill();
    }
  });
}

const createArcLine = (ctx: CanvasRenderingContext2D, cityFeatures:Feature[],
  projection: d3.GeoProjection) => {
  const len = cityFeatures.length;
  const lineString: GeoJSON.LineString[] = [];
  cityFeatures.forEach((f, i, d) => {
    if (f.geometry.type === "Point" && f.properties) {
      if (f.properties.POP_MAX < 2500000)
        return;
      const target = Math.floor(d3.randomNormal(1, len-1)());
      const targetF = d[target];
      if (target !== i && targetF
        && targetF.geometry && targetF.geometry.type === "Point") {
        if (targetF.properties && targetF.properties.POP_MAX < 2500000)
          return;
        const inter = d3.geoInterpolate(
          [f.geometry.coordinates[0],
           f.geometry.coordinates[1]],
          [targetF.geometry.coordinates[0],
           targetF.geometry.coordinates[1]]);
        const start = projection([
          f.geometry.coordinates[0],
          f.geometry.coordinates[1]]);
        const currentScale = projection.scale();
        projection.scale(600);
        const mid1 = projection(inter(.33));
        const mid2 = projection(inter(.76));
        projection.scale(currentScale);
        const end = projection([
          targetF.geometry.coordinates[0], targetF.geometry.coordinates[1]
        ]);
        if (start && mid1 && mid2 && end) {
          const obj: GeoJSON.LineString = {
            "type": "LineString",
            "coordinates": [ start, mid1, mid2, end ]
          };
          lineString.push(obj);
        }
        else {
          const obj: GeoJSON.LineString = {
            "type": "LineString",
            "coordinates": [
              f.geometry.coordinates,
              targetF.geometry.coordinates
            ]
          };
          lineString.push(obj);
        }
      }
    }
  });
  lineString.forEach((s) => {
    if (s.type === "LineString") {
      ctx.fillStyle = "none";
      ctx.strokeStyle = "yellow";
      ctx.lineWidth = .5;
      ctx.beginPath();
      ctx.moveTo(s.coordinates[0][0], s.coordinates[0][1]);
      ctx.bezierCurveTo(
        s.coordinates[1][0], s.coordinates[1][1],
        s.coordinates[2][0], s.coordinates[2][1],
        s.coordinates[3][0], s.coordinates[3][1]);
      ctx.stroke();
    }
  });
}

export default ArcLine;
