import React from 'react';

interface Prop {
  getRef: (node:HTMLCanvasElement) => void
}

class PureCanvas extends React.Component<Prop> {
  shouldComponentUpdate = () => {
    return false;
  }

  render() {
    return <canvas ref={node => node ? this.props.getRef(node) : null }></canvas>;
  }
}

export const initCanvas = (canvas:HTMLCanvasElement) => {
  const [width, height] = [canvas.clientWidth, canvas.clientHeight];
  // For Ratina Display
  const dpr = window.devicePixelRatio || 1;
  canvas.width = width * dpr;
  canvas.height = height * dpr;
  const context = canvas.getContext("2d");
  if (!context) return;
  context.globalCompositeOperation = "lighter";
  return context;
}

export default PureCanvas;
