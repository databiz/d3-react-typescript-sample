import React, { FC, useState } from 'react';
import { Feature, FeatureCollection } from 'geojson';
import * as d3 from 'd3';
import { GeoSphere, GeoPermissibleObjects } from 'd3';
import PureCanvas, { initCanvas } from './PureCanvas';

interface Props {
  projection: d3.GeoProjection;
  features: Feature | FeatureCollection;
}

interface State {
  context?: CanvasRenderingContext2D;
}

const Earth:FC<Props> = (props) => {
  const [state, setState] = useState<State>({});

  const saveRef = (canvas:HTMLCanvasElement) => {
    const context = initCanvas(canvas);
    if (context) {
      setState({
        ...state,
        context: context
      });
    }
  }

  if (state.context) {
    const pathGenerator = d3.geoPath<void, GeoPermissibleObjects>()
      .projection(props.projection)
      .context(state.context);

    state.context.save();
    state.context.clearRect(0, 0, state.context.canvas.clientWidth, state.context.canvas.clientHeight);
    createSea(state.context, pathGenerator);
    createLand(state.context, pathGenerator, props.features);
    state.context.restore();
  }

  return <PureCanvas getRef={saveRef}></PureCanvas>
}

const createLand = (ctx: CanvasRenderingContext2D,
  pathGenerator: d3.GeoPath<void, d3.GeoPermissibleObjects>,
  feature:Feature | FeatureCollection) => {
  const grey = 10 + 5 * 1 + 2 * Math.pow(10, 1.0 / 2.5);
  ctx.fillStyle = d3.rgb(grey, grey, grey).toString();
  ctx.strokeStyle = "gray";
  ctx.lineWidth = 0.1;
  ctx.beginPath();
  pathGenerator(feature);
  ctx.fill();
  ctx.stroke();
  ctx.restore();
}

const createSea = (ctx: CanvasRenderingContext2D,
  pathGenerator: d3.GeoPath<void, d3.GeoPermissibleObjects>) => {
  const sphere: GeoSphere = { type: "Sphere" };
  ctx.fillStyle = "rgba(30, 30, 30, 0.5)";
  ctx.beginPath();
  pathGenerator(sphere);
  ctx.fill();
  ctx.stroke();
}

export default Earth;
