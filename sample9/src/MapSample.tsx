import React, { Component } from 'react';
import * as d3 from 'd3';
import { SubjectPosition } from 'd3';
import { Feature } from 'geojson';
import * as topojson from 'topojson';
import { Topology } from 'topojson-specification';

import './MapSample.css';
import Earth from './components/Earth';
import ArcLine from './components/ArcLine';

interface MapFile {
  mapurl: string;
  dataurl: string;
  projection: d3.GeoProjection;
  objectsname: string;
  latitude: number;
  longitude: number;
}

interface MapState {
  mapdata: {
    status: "Loading";
  } | {
    status: "Success";
    topology: Topology;
    data: Topology;
  } | {
    status: "Failure";
    error: Error;
  };
  transform?: d3.ZoomTransform;
}

class MapSample extends Component<MapFile, MapState> {
  // Event用
  canvas: React.RefObject<HTMLCanvasElement>;

  constructor(props:MapFile) {
    super(props);
    this.canvas = React.createRef<HTMLCanvasElement>();
    this.state = {
      mapdata: {
        status: "Loading"
      }
    }
  }

  onDraged = () => {
    const rotate = this.props.projection.rotate();
    this.props.projection.rotate([d3.event.x, d3.event.y, rotate[2]]);
    this.setState({
      transform: { ...d3.event}
    });
  }

  onZoomed = () => {
    const transform = d3.event.transform;
    const p = this.props.projection([transform.x, transform.y]);
    if (p) {
      this.props.projection.translate(p);
      this.setState({
        transform: transform,
      });
    }
  }

  componentDidMount() {
    console.log("componentDidMount");
    // 地図データを取得する。
    // d3.jsはFetch APIを使用している。
    Promise.all([d3.json<Topology>(this.props.mapurl), d3.json<Topology>(this.props.dataurl)])
      .then(([map, data]) => {
        this.setState({
          mapdata: {
            status: "Success",
            topology: map,
            data: data,
          },
        });
      })
      .catch((error:Error) => {
        this.setState({
          mapdata: {
            status: "Failure",
            error: error,
          }});
      });
  }

  componentDidUpdate = () => {
    if (!this.canvas.current) return;
    const context = this.canvas.current.getContext('2d');
    if (!context) return;
    context.globalAlpha = 0.0;
    const canvas = d3.select<HTMLCanvasElement, Feature>(this.canvas.current);

    const drag = d3.drag<HTMLCanvasElement, Feature, SubjectPosition>()
      .subject(() => {
        const rotate = this.props.projection.rotate();
        return {x: rotate[0], y: rotate[1]}
      })
      .on("drag", this.onDraged);
    canvas.call(drag);

    const zoom = d3.zoom<HTMLCanvasElement, Feature>()
      .scaleExtent([1, 24]) // 範囲
      .on("zoom", this.onZoomed);
    canvas.call(zoom);
  }

  render() {
    if (this.state.mapdata.status === "Loading") {
      return <p>Loading...</p>
    }
    else if (this.state.mapdata.status === "Failure") {
      return <p>Error: {this.state.mapdata.error.message}</p>
    }
    else {
      return (
        <div className="canvas-wrapper">
          <canvas className="background"></canvas>
          <Earth
            projection={this.props.projection}
            features={topojson.feature(
              this.state.mapdata.topology,
              this.state.mapdata.topology.objects[this.props.objectsname]) }></Earth>
          <ArcLine
            projection={this.props.projection}
            features={topojson.feature(
              this.state.mapdata.data,
              this.state.mapdata.data.objects[this.props.objectsname]) }></ArcLine>
          <canvas ref={(this.canvas)}></canvas>
        </div>)
    }
  }
}

export default MapSample;
