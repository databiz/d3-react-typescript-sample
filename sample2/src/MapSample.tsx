import React, { Component } from 'react';
import * as d3 from 'd3';
import * as topojson from 'topojson';
import { Topology } from 'topojson-specification';
import { Feature } from 'geojson';

import './MapSample.css';
import { GeoSphere } from 'd3';

interface MapFile {
  url: string;
  objectsname: string;
  latitude: number;
  longitude: number;
}

interface MapState {
  mapdata: {
    status: "Loading";
  } | {
    status: "Success";
    topology: Topology;
  } | {
    status: "Failure";
    error: Error;
  };
}

class MapSample extends Component<MapFile, MapState> {
  // d3.select("svg")は他のコンポーネントのDOMまで取得するため使用しない。
  // 代わりにrefから取得したsvgを使用する。
  svg: React.RefObject<SVGSVGElement>;

  constructor(props:MapFile) {
    super(props);
    this.svg = React.createRef<SVGSVGElement>();
    this.state = {
      mapdata: {
        status: "Loading"
      }
    }
  }

  // 投影法を取得する。
  getProjection = ():d3.GeoProjection => {
    let width = 0;
    let height = 0;
    if (this.svg.current) {
      width = this.svg.current.clientWidth;
      height = this.svg.current.clientHeight;
    }
    return d3.geoOrthographic()
      .scale(300)
      .translate([width / 2, height / 2]);
  }

  // 地図を描画する。
  draw = (svg:d3.Selection<SVGSVGElement, Feature[], null, undefined>) => {
    if (this.state.mapdata.status !== "Success") return;
    const mapData = this.state.mapdata.topology;

    // 投影法の設定を取得する。
    const projection = this.getProjection();
    // 地理情報のPathGeneratorを取得する。
    const pathGenerator = d3.geoPath()
      .projection(projection);

    // 取得した地図データをDOM要素に設定する。
    const feature = topojson
      .feature(mapData, mapData.objects[this.props.objectsname]);
    const features:Feature[] = (feature.type === "FeatureCollection") ?
			feature.features : [feature];

    const g = svg.append('g');

    /* #@@range_begin(sphere_sample)  */
    // 海を描画する。
    // type: Sphere
    const sphere:GeoSphere[] = [{type: "Sphere"}];
    const sea = g.selectAll(".sea")
      .data(sphere);
    sea.enter().append("path")
      .attr("class", "shape sea")
      .attr("d", pathGenerator)
      .style("fill", "blue")
      .style("fill-opacity", 0.2);
		/* #@@range_end(sphere_sample)  */

    const item = g.selectAll(".item")
      .data(features);

    // 存在しないデータのDOM要素を削除する。
    item.exit()
      .remove();

    // 指定した地図データを元に地図を描画する。
    item.enter().append("path")
      .attr("class", "shape item")
      .attr("d", pathGenerator)
      .style("fill", "black")
      .style("stroke", () => { return "white"; })
      .style("stroke-width", () => { return 0.1; });
  }

  componentDidMount() {
    console.log("componentDidMount");
    // 地図データを取得する。
    // d3.jsはFetch APIを使用している。
    d3.json<Topology>(this.props.url)
      .then((topology:Topology) => {
        this.setState({
          mapdata: {
            status: "Success",
            topology: topology,
          }});
      })
      .catch((error:Error) => {
        this.setState({
          mapdata: {
            status: "Failure",
            error: error,
          }});
      });
  }

  componentDidUpdate = () => {
    console.log("componentDidUpdate");
    if (!this.svg.current) return;
    const svg = d3.select<SVGSVGElement, Feature[]>(this.svg.current);
    this.draw(svg);
  }

  render() {
    if (this.state.mapdata.status === "Loading") {
      return <p>Loading...</p>
    }
    else if (this.state.mapdata.status === "Failure") {
      return <p>Error: {this.state.mapdata.error.message}</p>
    }
    else {
      return <svg ref={(this.svg)}></svg>
    }
  }
}

export default MapSample;
