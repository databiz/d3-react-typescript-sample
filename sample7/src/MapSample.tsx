import React, { Component } from 'react';
import * as d3 from 'd3';
import * as topojson from 'topojson';
import { Topology } from 'topojson-specification';
import { Feature } from 'geojson';

import './MapSample.css';
import { GeoSphere, SubjectPosition, GeoPermissibleObjects} from 'd3';

/* #@@range_begin(line_sample2) */
interface MapFile {
  mapurl: string;
  objectsname: string;
  latitude: number;
  longitude: number;
}
/* #@@range_end(line_sample2) */

interface MapState {
  mapdata: {
    status: "Loading";
  } | {
    status: "Success";
    topology: Topology;
  } | {
    status: "Failure";
    error: Error;
  };
}

class MapSample extends Component<MapFile, MapState> {
  // d3.select("svg")は他のコンポーネントのDOMまで取得するため使用しない。
  // 代わりにrefから取得したsvgを使用する。
  svg: React.RefObject<SVGSVGElement>;

  constructor(props:MapFile) {
    super(props);
    this.svg = React.createRef<SVGSVGElement>();
    this.state = {
      mapdata: {
        status: "Loading"
      }
    }
  }

  // 投影法を取得する。
  getProjection = ():d3.GeoProjection => {
    let width = 0;
    let height = 0;
    if (this.svg.current) {
      width = this.svg.current.clientWidth;
      height = this.svg.current.clientHeight;
    }
    // return d3.geoEquirectangular()
    // return d3.geoMercator()
    /* #@@range_begin(line_sample3) */
    return d3.geoOrthographic()
      .scale(300)
      .rotate([-this.props.latitude, -this.props.longitude, 0])
      .translate([width / 2, height / 2]);
    /* #@@range_end(line_sample3) */
  }

  // 地図を描画する。
  draw = (svg:d3.Selection<SVGSVGElement, Feature, null, undefined>) => {
    if (this.state.mapdata.status !== "Success") return;
    const mapData = this.state.mapdata.topology;

    // 投影法の設定を取得する。
    const projection = this.getProjection();
    // 地理情報のPathGeneratorを取得する。
    const pathGenerator = d3.geoPath<SVGPathElement, GeoPermissibleObjects>()
      .projection(projection);

    // 取得した地図データをDOM要素に設定する。
    const feature = topojson
      .feature(mapData, mapData.objects[this.props.objectsname]);
    const features:Feature[] = (feature.type === "FeatureCollection") ?
      feature.features : [feature];

    /* #@@range_begin(plot_pop_sample1_1) */
    const [popMin, popMax] = d3.extent(features, (f) => {
      return (f.properties) ? +f.properties.POP_EST : undefined;
    });
    const popScale = (popMin && popMax)
       ? d3.scaleSqrt().domain([popMin, popMax]).range([0, 1])
       : undefined;
    /* #@@range_end(plot_pop_sample1_1) */

    const g = svg.append('g');

    // 海を描画する。
    // type: Sphere
    const sphere:GeoSphere[] = [{type: "Sphere"}];
    const sea = g.selectAll(".sea")
      .data(sphere);
    sea.enter().append("path")
      .attr("class", "shape sea")
      .attr("d", pathGenerator)
      .style("fill", "blue")
      .style("fill-opacity", 0.2);

    const item = g.selectAll(".item")
      .data(features);

    // 存在しないデータのDOM要素を削除する。
    item.exit()
      .remove();

    /* #@@range_begin(plot_pop_sample1_2) */
    // 指定した地図データを元に地図を描画する。
    item.enter().append("path")
      .attr("class", "shape item")
      .attr("d", pathGenerator)
      .style("fill", d => { return this.getCountryColor(d, popScale); })
      .style("stroke", () => { return "gray"; })
      .style("stroke-width", () => { return 0.1; })
      .on("mouseover", (d,i,elem1) => {
        // 棒グラフの要素を取得して地図の要素が一致する場合に強調する。
        d3.selectAll<SVGRectElement, Feature>(".bar")
          .style("fill", (d2,j,elem2) => {
            return (this.equalsFeature(d, d2)) ? "red" : elem2[j].style.fill;
          });
        // 地図の要素を強調する。
        d3.select<SVGPathElement, Feature>(elem1[i])
          .style("fill", "red")
          .style("stroke", "black")
          .style("stroke-width", 2.0);
      })
      .on("mouseout", (d,i,elem1) => {
        // 棒グラフの要素を取得して地図の要素が一致する場合に強調を解除する。
        d3.selectAll<SVGRectElement, Feature>(".bar")
          .style("fill", (d2,j,elem2) => {
            return (this.equalsFeature(d, d2))
              ? "steelblue" : elem2[j].style.fill;
          });
        // 地図の要素を強調を解除する。
        d3.select<SVGPathElement, Feature>(elem1[i])
          .style("fill", d => { return this.getCountryColor(d, popScale); })
          .style("stroke", "gray")
          .style("stroke-width", 0.1);
      });
      /* #@@range_end(plot_pop_sample1_2) */

		const drag = d3.drag<SVGSVGElement, Feature, SubjectPosition>()
			.subject(() => {
				const rotate = projection.rotate();
				return {x: rotate[0], y: -rotate[1]}
			})
			.on("drag", () => { this.onDraged(projection, pathGenerator) });
		svg.call(drag);

		const zoom = d3.zoom<SVGSVGElement, Feature>()
			.scaleExtent([1, 24]) // 範囲
			.on("zoom", this.onZoomed);
    svg.call(zoom);

    /* #@@range_begin(plot_mouseover_sample1_1) */
    if(popScale) {
      this.barChart(svg, features, popScale);
    }
    /* #@@range_end(plot_mouseover_sample1_1) */
  }

  getCountryColor = (d:Feature, scale?:d3.ScaleContinuousNumeric<number, number>) => {
    return (d.properties && scale)
    ? d3.interpolateReds(scale(+d.properties.POP_EST))
    : "gray";
  };

  /* #@@range_begin(plot_mouseover_sample1_2) */
  // 2つのFeatureが一致するか判定する。
  equalsFeature = (d:Feature, d2:Feature) => {
    return (d.properties && d2.properties
      && (d.properties.NAME === d2.properties.NAME));
  };
  /* #@@range_end(plot_mouseover_sample1_2) */

  barChart = (svg:d3.Selection<SVGSVGElement, Feature, null, undefined>,
    features:Feature[], popScale:d3.ScalePower<number, number>) => {

    let width = 0;
    let height = 0;
    if (this.svg.current) {
      width = this.svg.current.clientWidth;
      height = this.svg.current.clientHeight;
    }
    const barHeight = height / 2;

    const x:d3.ScaleBand<string> = d3.scaleBand().rangeRound([0, width]).padding(0.1);
    const y = d3.scaleSqrt().range([height, barHeight]);

    const sortedFeature = features
      .sort((a, b) => {
        return (a.properties && b.properties &&
          +a.properties.GDP_MD_EST <= +b.properties.GDP_MD_EST) ? 1 : -1});
    x.domain(sortedFeature.map(d => {
      return (d.properties) ? d.properties.NAME : null;
    }));
    const max = d3.max(sortedFeature, (f) => {
      return (f.properties) ? +f.properties.GDP_MD_EST : 0;
    });
    y.domain([0, max || 0]);

    var yAxis = d3.axisLeft(y)
      .scale(y)
      .ticks(10);
    const barArea = svg.append("g")
      .attr("transform", "translate(0, -10)")
    barArea.append("g")
      .attr("class", "y axis")
      .attr("transform", "translate(70, 0)")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Value ($)");

    /* #@@range_begin(plot_mouseover_sample1_3) */
    // 棒グラフを作成します。
    const bar = barArea.selectAll(".bar")
      .data(sortedFeature);
    bar.enter().append("rect")
      .attr("class", "bar")
      .attr("fill", "steelblue")
      .attr("x", d => {
        return (d.properties) ? (x(d.properties.NAME) || null) : null;
      }) // 型エラー対応；undefinedだったらnullを返却する
      .attr("y", d => { return (d.properties)
        ? y(+d.properties.GDP_MD_EST) : barHeight;
      })
      .attr("width", x.bandwidth())
      .attr("height", d => {
        return height - ((d.properties)
          ? y(+d.properties.GDP_MD_EST) : barHeight);
      })
      .on("mouseover", (d,i,elem1) => {
        d3.selectAll<SVGPathElement, Feature>(".item")
          .style("fill", (d2,j,elem2) => {
            return (this.equalsFeature(d, d2))
              ? "red" :  elem2[j].style.fill;
          })
          .style("stroke", (d2,j,elem2) => {
            return (this.equalsFeature(d, d2))
              ? "black" : elem2[j].style.stroke;
          })
          .style("stroke-width", (d2,j,elem2) => {
            return (this.equalsFeature(d, d2))
              ? 2.0 : elem2[j].style.strokeWidth;
          });
        d3.select(elem1[i])
            .style("fill", "red");
      })
      .on("mouseout", (d,i,elem1) => {
        d3.selectAll<SVGPathElement, Feature>(".item")
          .style("fill", (d2,j,elem2) => {
            const color = this.getCountryColor(d, popScale);
            return (this.equalsFeature(d, d2))
              ?  color : elem2[j].style.fill;
          })
          .style("stroke", (d2,j,elem2) => {
            return (this.equalsFeature(d, d2))
              ? "gray" : elem2[j].style.stroke;
          })
          .style("stroke-width", (d2,j,elem2) => {
            return (this.equalsFeature(d, d2))
              ? 0.1 : elem2[j].style.strokeWidth;
          });
        d3.select(elem1[i])
            .style("fill", "steelblue");
      });
      /* #@@range_end(plot_mouseover_sample1_3) */
  }

  onDraged = (projection:d3.GeoProjection,
    pathGenerator:d3.GeoPath<SVGPathElement, GeoPermissibleObjects>) => {
    const rotate = projection.rotate();
    projection.rotate([d3.event.x, -d3.event.y, rotate[2]]);
    if (!this.svg.current) return;
    const svg = d3.select<SVGSVGElement, Feature>(this.svg.current);
    svg.selectAll<SVGPathElement, GeoPermissibleObjects>("path")
      .attr("d", pathGenerator);
  }

  onZoomed = () => {
    if (!this.svg.current) return;
    d3.select<SVGSVGElement, Feature>(this.svg.current)
      .selectAll(".shape")
      .attr("transform", d3.event.transform);
  }

  componentDidMount() {
    console.log("componentDidMount");
    // 地図データを取得する。
    // d3.jsはFetch APIを使用している。
    d3.json<Topology>(this.props.mapurl)
      .then((topology) => {
        this.setState({
          mapdata: {
            status: "Success",
            topology: topology,
          },
        });
      })
      .catch((error:Error) => {
        this.setState({
          mapdata: {
            status: "Failure",
            error: error,
          }});
      });
  }

  componentDidUpdate = () => {
    console.log("componentDidUpdate");
    if (!this.svg.current) return;
    const svg = d3.select<SVGSVGElement, Feature>(this.svg.current);
    this.draw(svg);
  }

  render() {
    if (this.state.mapdata.status === "Loading") {
      return <p>Loading...</p>
    }
    else if (this.state.mapdata.status === "Failure") {
      return <p>Error: {this.state.mapdata.error.message}</p>
    }
    else {
      return (
        <>
          {/* <div className="country_info">
            Name: { this.state.country.name }<br />
            GDP: { this.state.country.gdp }<br />
            Population: { this.state.country.population }
          </div> */}
          <svg ref={(this.svg)}></svg>
        </>)
    }
  }
}

export default MapSample;
