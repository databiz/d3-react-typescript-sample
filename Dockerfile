ARG NODE_VERSION=12.6.0

FROM node:$NODE_VERSION-alpine as node
FROM osgeo/gdal:alpine-ultrasmall-latest 

LABEL maintainer="aradgyma@gmail.com"

COPY --from=node /usr/local/bin/node /usr/local/bin/
COPY --from=node /usr/local/lib/node_modules/npm /usr/local/lib/node_modules/npm
RUN ln -s /usr/local/lib/node_modules/npm/bin/npm-cli.js /usr/local/bin/npm \
  && ln -s /usr/local/lib/node_modules/npm/bin/npx-cli.js /usr/local/bin/npx

ARG TOPOJSON_VERSION=3.0.2
WORKDIR /work
RUN npm i -g topojson@$TOPOJSON_VERSION