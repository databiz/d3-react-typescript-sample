import React, { useState, useEffect, useRef } from 'react';
import * as d3 from 'd3';
import { GeoSphere } from 'd3';
import * as topojson from 'topojson';
import { Topology } from 'topojson-specification';
import { Feature } from 'geojson';

import './MapSample.css';

interface MapFile {
  url: string;
  objectsname: string;
}

interface MapState {
  mapdata: {
    status: "Loading";
  } | {
    status: "Success";
    topology: Topology;
  } | {
    status: "Failure";
    error: Error;
  };
}

const MapSample: React.FC<MapFile> = (props:MapFile) => {
  const svg = useRef<SVGSVGElement>(null);
  const [mapState, setMapState] = useState<MapState>({ mapdata:{ status: "Loading" }});

  useEffect(() => {
    console.log("useEffect:getMapData");
    // 地図データを取得する。
    // d3.jsはFetch APIを使用している。
    d3.json<Topology>(props.url)
      .then((topology:Topology) => {
        setMapState({
          mapdata: {
            status: "Success",
            topology: topology,
          }});
      })
      .catch((error:Error) => {
        setMapState({
          mapdata: {
            status: "Failure",
            error: error,
          }});
      });
  }, [props.url]);

  useEffect(() => {
    console.log("useEffect:draw");
    // 投影法を取得する。
    const getProjection = ():d3.GeoProjection => {
      let width = 0;
      let height = 0;
      if (svg.current) {
        width = svg.current.clientWidth;
        height = svg.current.clientHeight;
      }
      return d3.geoOrthographic()
        .scale(300)
        .translate([width / 2, height / 2]);
    }

    // 地図を描画する。
    const draw = () => {
      if (!svg.current) return;
      const d3svg = d3.select<SVGSVGElement, Feature[]>(svg.current);
      if (mapState.mapdata.status !== "Success") return;
      const mapData = mapState.mapdata.topology;

      // 投影法の設定を取得する。
      const projection = getProjection();
      // 地理情報のPathGeneratorを取得する。
      const pathGenerator = d3.geoPath()
        .projection(projection)
        .pointRadius(1.5);

      // 取得した地図データをDOM要素に設定する。
      const feature = topojson
        .feature(mapData, mapData.objects[props.objectsname]);
      const features:Feature[] = (feature.type === "FeatureCollection") ?
        feature.features : [feature];

      const g = d3svg.append('g');

      // 海を描画する。
      // type: Sphere
      const sphere:GeoSphere[] = [{type: "Sphere"}];
      const sea = g.selectAll(".sea")
        .data(sphere);
      sea.enter().append("path")
        .attr("class", "shape sea")
        .attr("d", pathGenerator)
        .style("fill", "blue")
        .style("fill-opacity", 0.2);

      const item = g.selectAll(".item")
        .data(features);

      // 存在しないデータのDOM要素を削除する。
      item.exit()
        .remove();

      // 指定した地図データを元に地図を描画する。
      item.enter().append("path")
        .attr("class", "shape item")
        .attr("d", pathGenerator)
        .style("fill", "black")
        .style("stroke", () => { return "white"; })
        .style("stroke-width", () => { return 0.1; });
    }

    if(svg.current) {
      draw();
    }
  }, [props.objectsname, mapState.mapdata]);

  if (mapState.mapdata.status === "Loading") {
    return <p>Loading...</p>
  }
  else if (mapState.mapdata.status === "Failure") {
    return <p>Error: {mapState.mapdata.error.message}</p>
  }
  else {
    return <svg ref={(svg)}></svg>
  }
}

export default MapSample;
