import React, { Component } from 'react';
import * as d3 from 'd3';
import * as topojson from 'topojson';
import { Topology } from 'topojson-specification';
import { Feature } from 'geojson';

import './MapSample.css';

interface MapFile {
  url: string;
  objectsname: string;
}

/* #@@range_begin(fetch_sample1) */
interface MapState {
  mapdata: {
    status: "Loading";
  } | {
    status: "Success";
    topology: Topology;
  } | {
    status: "Failure";
    error: Error;
  };
}
/* #@@range_end(fetch_sample1) */

class MapSample extends Component<MapFile, MapState> {
  // d3.select("svg")は他のコンポーネントのDOMまで取得するため使用しない。
  // Reactが管理しているDOM要素を使用するため、refから取得したsvgを使用する。
	/* #@@range_begin(ref_sample1) */
 	svg: React.RefObject<SVGSVGElement>;

  constructor(props:MapFile) {
    super(props);
    this.svg = React.createRef<SVGSVGElement>();
		/* #@@range_end(ref_sample1) */
    this.state = {
      mapdata: {
        status: "Loading"
      }
    }
  }

	/* #@@range_begin(draw_sample1) */
  // 投影法を取得する。
  getProjection = ():d3.GeoProjection => {
    let width = 0;
    let height = 0;
    if (this.svg.current) {
      width = this.svg.current.clientWidth;
      height = this.svg.current.clientHeight;
    }
    return d3.geoOrthographic()
      .scale(300)
      .translate([width / 2, height / 2]);
  }
	/* #@@range_end(draw_sample1) */

  // 地図を描画する。
  draw = (svg:d3.Selection<SVGSVGElement, Feature[], null, undefined>) => {
    if (this.state.mapdata.status !== "Success") return;
    const mapData = this.state.mapdata.topology;

		/* #@@range_begin(draw_sample2) */
    // 投影法の設定を取得する。
    const projection = this.getProjection();
    // 地理情報のPathGeneratorを取得する。
    const pathGenerator = d3.geoPath()
      .projection(projection);
		/* #@@range_end(draw_sample2) */

		/* #@@range_begin(draw_sample3) */
    // 取得した地図データをDOM要素に設定する。
    const feature = topojson
      .feature(mapData, mapData.objects[this.props.objectsname]);
    const features:Feature[] = (feature.type === "FeatureCollection") ?
      feature.features : [feature];

    const g = svg.append('g');

    const item = g.selectAll(".item")
      .data(features);

    // 存在しないデータのDOM要素を削除する。
    item.exit()
      .remove();

    // 指定した地図データを元に地図を描画する。
    item.enter().append("path")
      .attr("class", "shape item")
      .attr("d", pathGenerator)
      .style("fill", "black")
      .style("stroke", () => { return "white"; })
      .style("stroke-width", () => { return 0.1; });
		/* #@@range_end(draw_sample3) */
	}
	
	/* #@@range_begin(fetch_sample2) */
  componentDidMount() {
    console.log("componentDidMount");
    // 地図データを取得する。
    // d3.jsはFetch APIを使用している。
    d3.json<Topology>(this.props.url)
      .then((topology:Topology) => {
        this.setState({
          mapdata: {
            status: "Success",
            topology: topology,
          }});
      })
      .catch((error:Error) => {
        this.setState({
          mapdata: {
            status: "Failure",
            error: error,
          }});
      });
  }
	/* #@@range_end(fetch_sample2) */

	/* #@@range_begin(render_sample) */
  componentDidUpdate = () => {
    console.log("componentDidUpdate");
    if (!this.svg.current) return;
    const svg = d3.select<SVGSVGElement, Feature[]>(this.svg.current);
    this.draw(svg);
  }
	/* #@@range_end(render_sample) */

/* #@@range_begin(ref_sample2_1) */
  render() {
		/* #@@range_end(ref_sample2_1) */
    if (this.state.mapdata.status === "Loading") {
      return <p>Loading...</p>
    }
    else if (this.state.mapdata.status === "Failure") {
      return <p>Error: {this.state.mapdata.error.message}</p>
    }
    else {
			/* #@@range_begin(ref_sample2_2) */
      return <svg ref={(this.svg)}></svg>
			/* #@@range_end(ref_sample2_2) */
    }
  }
}

export default MapSample;
